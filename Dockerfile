FROM ubuntu 


RUN mkdir -p /root/.ssh/
COPY id_rsa /root/.ssh/
RUN chmod 700 /root/.ssh/
RUN chmod 600 /root/.ssh/id_rsa



RUN apt-get -y update && apt-get -y upgrade
RUN apt-get -y install ansible python-pip unzip nano git

RUN wget https://releases.hashicorp.com/terraform/0.11.8/terraform_0.11.8_linux_amd64.zip
RUN unzip terraform_0.11.8_linux_amd64.zip
RUN mv terraform /usr/local/bin/



RUN pip install awscli

RUN pip install ansible-lint

RUN touch /root/.ssh/known_hosts


CMD ["/bin/bash"]